package BP.DA;

import BP.Pub.*;
import BP.Sys.*;

import java.io.*;
import java.time.*;


///#region enum LogType

/**
 * 信息类型
 */
public enum LogType {
    /**
     * 提示
     */
    Info(1),
    /**
     * 警告
     */
    Warning(2),
    /**
     * 错误
     */
    Error(3),
    /*
     * debug
     * */
    Debug(4);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, LogType> mappings;
    private int intValue;

    private LogType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, LogType> getMappings() {
        if (mappings == null) {
            synchronized (LogType.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, LogType>();
                }
            }
        }
        return mappings;
    }

    public static LogType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}

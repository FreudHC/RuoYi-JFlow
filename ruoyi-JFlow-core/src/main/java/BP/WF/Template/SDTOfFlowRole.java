package BP.WF.Template;

import BP.WF.*;

/**
 * 流程计划完成日期计算规则
 */
public enum SDTOfFlowRole {
    /**
     * 不计算
     */
    None,
    /**
     * 按照指定的字段计算
     */
    BySpecDateField,
    /**
     * 按照sql
     */
    BySQL,
    /**
     * 所有的节点之和.
     */
    ByAllNodes,
    /**
     * 按照设置的天数
     */
    ByDays;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static SDTOfFlowRole forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}

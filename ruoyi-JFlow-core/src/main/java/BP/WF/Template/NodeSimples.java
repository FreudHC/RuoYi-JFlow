package BP.WF.Template;

import BP.DA.*;
import BP.Sys.*;
import BP.En.*;
import BP.Port.*;
import BP.WF.Data.*;
import BP.WF.Template.*;
import BP.WF.Port.*;
import BP.WF.*;

import java.util.*;

/**
 * 节点集合
 */
public class NodeSimples extends EntitiesOIDName {

    ///#region 方法

    /**
     * 节点集合
     */
    public NodeSimples() {
    }

    ///#endregion


    ///#region 构造方法

    /**
     * 节点集合.
     *
     * @param FlowNo
     * @throws Exception
     */
    public NodeSimples(String fk_flow) throws Exception {
        this.Retrieve(NodeAttr.FK_Flow, fk_flow, NodeAttr.Step);
    }

    /**
     * 得到它的 Entity
     */
    @Override
    public Entity getNewEntity() {
        return new NodeSimple();
    }

    ///#endregion


    ///#region 为了适应自动翻译成java的需要,把实体转换成List.

    /**
     * 转化成 java list,C#不能调用.
     *
     * @return List
     */
    public final List<NodeSimple> ToJavaList() {
        return (List<NodeSimple>) (Object) this;
    }

    /**
     * 转化成list 为了翻译成java的需要
     *
     * @return List
     */
    public final ArrayList<BP.WF.Template.NodeSimple> Tolist() {
        ArrayList<BP.WF.Template.NodeSimple> list = new ArrayList<BP.WF.Template.NodeSimple>();
        for (int i = 0; i < this.size(); i++) {
            list.add((BP.WF.Template.NodeSimple) this.get(i));
        }
        return list;
    }

    ///#endregion 为了适应自动翻译成java的需要,把实体转换成List.

}

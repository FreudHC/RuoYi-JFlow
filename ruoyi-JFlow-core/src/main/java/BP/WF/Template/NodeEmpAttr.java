package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import BP.WF.*;

import java.util.*;

/**
 * 节点人员属性
 */
public class NodeEmpAttr {
    /**
     * 节点
     */
    public static final String FK_Node = "FK_Node";
    /**
     * 人员
     */
    public static final String FK_Emp = "FK_Emp";
}

package BP.WF.Rpt;

import BP.DA.DataType;
import BP.WF.*;

import java.util.*;
import java.io.*;
import java.time.*;

/**
 * 报表导出模板字段与单元格绑定信息对象
 */
public class RptExportTemplateCell {
    //C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [XmlIgnore] private string _cellName;
    private String _cellName;

    /**
     * 单元格行号
     */
    private int RowIdx;
    /**
     * 单元格列号
     */
    private int ColumnIdx;
    /**
     * 单元格所属sheet表名
     */
    private String SheetName;
    /**
     * 字段所属fk_mapdata
     */
    private String FK_MapData;
    /**
     * 字段英文名
     */
    private String KeyOfEn;
    /**
     * 明细表字段所属fk_mapdata
     */
    private String FK_DtlMapData;
    /**
     * 明细表字段英文名
     */
    private String DtlKeyOfEn;

    /**
     * 获取单元格名称
     */
    public final String getCellName() {
        if (DataType.IsNullOrEmpty(_cellName)) {
            _cellName = GetCellName(getColumnIdx(), getRowIdx());
        }

        return _cellName;
    }

    /**
     * 获取单元格的显示名称，格式如A1,B2
     *
     * @param columnIdx 单元格列号
     * @param rowIdx    单元格行号
     * @return
     */
    public static String GetCellName(int columnIdx, int rowIdx) {
        int[] maxs = new int[]{26, 26 * 26 + 26, 26 * 26 * 26 + (26 * 26 + 26) + 26};
        int col = columnIdx + 1;
        int row = rowIdx + 1;

        if (col > maxs[2]) {
            throw new RuntimeException("列序号不正确，超出最大值");
        }

        int alphaCount = 1;

        for (int m : maxs) {
            if (m < col) {
                alphaCount++;
            }
        }

        switch (alphaCount) {
            case 1:
                return (char) (col + 64) + "" + row;
            case 2:
                return (char) ((col / 26) + 64) + "" + (char) ((col % 26) + 64) + row;
            case 3:
                return (char) ((col / 26 / 26) + 64) + "" + (char) (((col - col / 26 / 26 * 26 * 26) / 26) + 64) + (char) ((col % 26) + 64) + row;
        }

        return "Unkown";
    }

    public final int getColumnIdx() {
        return ColumnIdx;
    }

    public final int getRowIdx() {
        return RowIdx;
    }

    public final void setRowIdx(int value) {
        RowIdx = value;
    }

    public final void setColumnIdx(int value) {
        ColumnIdx = value;
    }

    public final String getSheetName() {
        return SheetName;
    }

    public final void setSheetName(String value) {
        SheetName = value;
    }

    public final String getFK_MapData() {
        return FK_MapData;
    }

    public final void setFK_MapData(String value) {
        FK_MapData = value;
    }

    public final String getKeyOfEn() {
        return KeyOfEn;
    }

    public final void setKeyOfEn(String value) {
        KeyOfEn = value;
    }

    public final String getFK_DtlMapData() {
        return FK_DtlMapData;
    }

    public final void setFK_DtlMapData(String value) {
        FK_DtlMapData = value;
    }

    public final String getDtlKeyOfEn() {
        return DtlKeyOfEn;
    }

    public final void setDtlKeyOfEn(String value) {
        DtlKeyOfEn = value;
    }
}

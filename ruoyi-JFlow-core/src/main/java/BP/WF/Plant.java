package BP.WF;

/**
 * 运行平台
 */
public enum Plant {
    CCFlow,
    JFlow;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static Plant forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}

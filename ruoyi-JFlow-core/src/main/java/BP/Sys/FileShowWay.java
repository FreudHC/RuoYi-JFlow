package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 文件展现方式
 */
public enum FileShowWay {
    /**
     * 表格
     */
    Table,
    /**
     * 图片
     */
    Pict,
    /**
     * 自由模式
     */
    Free;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static FileShowWay forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}

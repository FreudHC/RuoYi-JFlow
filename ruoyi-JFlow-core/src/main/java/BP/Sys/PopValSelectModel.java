package BP.Sys;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;

import java.util.*;

/**
 * 选择模式
 */
public enum PopValSelectModel {
    /**
     * 单选
     */
    One,
    /**
     * 多选
     */
    More;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static PopValSelectModel forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}

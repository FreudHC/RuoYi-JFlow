package BP.Sys;


/**
 * 运行平台
 */
public enum Plant {
    /**
     * 默认不打开.
     */
    CSharp,
    /**
     * 打开
     */
    Java;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static Plant forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}

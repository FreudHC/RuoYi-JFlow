package BP.Sys.XML;

import BP.DA.*;
import BP.En.*;
import BP.Sys.XML.*;
import BP.Sys.*;

/**
 * 属性
 */
public class SQLListAttr {
    /**
     * 编号
     */
    public static final String No = "No";
    /**
     * SQL
     */
    public static final String SQL = "SQL";
    /**
     * 备注
     */
    public static final String Note = "Note";

}

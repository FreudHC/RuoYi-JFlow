package BP.Sys.XML;

import BP.DA.*;
import BP.En.*;
import BP.Sys.XML.*;
import BP.Sys.*;

/**
 * 属性
 */
public class EnumInfoXmlAttr {
    /**
     * 编号
     */
    public static final String No = "No";
    /**
     * 名称
     */
    public static final String Name = "Name";
    /**
     * 描述
     */
    public static final String Vals = "Vals";
}

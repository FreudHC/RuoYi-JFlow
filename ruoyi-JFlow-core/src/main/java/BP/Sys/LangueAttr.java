package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 语言Attr
 */
public class LangueAttr {
    /**
     * 模块
     */
    public static final String Model = "Model";
    /**
     * 分类
     */
    public static final String Sort = "Sort";
    /**
     * 关联的键
     */
    public static final String SortKey = "SortKey";
    /**
     * 模块
     */
    public static final String Langue = "Langue";
    /**
     * 值
     */
    public static final String Val = "Val";
    /**
     * 模块键值 FrmID, Or FlowNo
     */
    public static final String ModelKey = "ModelKey";
}

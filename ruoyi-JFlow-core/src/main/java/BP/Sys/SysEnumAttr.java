package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * sss
 */
public class SysEnumAttr {
    /**
     * 标题
     */
    public static final String Lab = "Lab";
    /**
     * Int key
     */
    public static final String IntKey = "IntKey";
    /**
     * EnumKey
     */
    public static final String EnumKey = "EnumKey";
    /**
     * Language
     */
    public static final String Lang = "Lang";
}

package BP.GPM;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 菜单
 */
public class MenuAttr extends EntityTreeAttr {
    /**
     * 控制方法
     */
    public static final String MenuCtrlWay = "MenuCtrlWay";
    /**
     * 系统
     */
    public static final String FK_App = "FK_App";
    /**
     * 图片
     */
    public static final String Img = "Img";
    /**
     * 连接
     */
    public static final String Url = "Url";
    /**
     * 连接
     */
    public static final String UrlExt = "UrlExt";
    /**
     * 控制内容
     */
    public static final String CtrlObjs = "CtrlObjs";
    /**
     * 是否启用
     */
    public static final String IsEnable = "IsEnable";
    /**
     * 打开方式
     */
    public static final String OpenWay = "OpenWay";
    /**
     * 标记
     */
    public static final String Flag = "Flag";
    /**
     * 扩展1
     */
    public static final String Tag1 = "Tag1";
    /**
     * 扩展2
     */
    public static final String Tag2 = "Tag2";
    /**
     * Tag3
     */
    public static final String Tag3 = "Tag3";
    /**
     * 图标
     */
    public static final String Icon = "Icon";
}

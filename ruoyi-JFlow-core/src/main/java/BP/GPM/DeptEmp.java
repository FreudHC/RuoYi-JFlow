package BP.GPM;

import BP.DA.*;
import BP.En.*;
import BP.En.Map;

import java.util.*;

/**
 * 部门人员信息 的摘要说明。
 */
public class DeptEmp extends EntityMyPK {

    ///#region 基本属性

    /**
     * 工作部门人员信息
     */
    public DeptEmp() {
    }

    /**
     * 查询
     *
     * @param deptNo 部门编号
     * @param empNo  人员编号
     * @throws Exception
     */
    public DeptEmp(String deptNo, String empNo) throws Exception {
        this.setFK_Dept(deptNo);
        this.setFK_Emp(empNo);
        this.setMyPK(this.getFK_Dept() + "_" + this.getFK_Emp());
        this.Retrieve();
    }

    /**
     * 部门
     *
     * @throws Exception
     */
    public final String getFK_Dept() throws Exception {
        return this.GetValStringByKey(DeptEmpAttr.FK_Dept);
    }

    /**
     * 人员
     *
     * @throws Exception
     */
    public final String getFK_Emp() throws Exception {
        return this.GetValStringByKey(DeptEmpAttr.FK_Emp);
    }

    public final void setFK_Emp(String value) throws Exception {
        SetValByKey(DeptEmpAttr.FK_Emp, value);
        this.setMyPK(this.getFK_Dept() + "_" + this.getFK_Emp());
    }

    ///#endregion


    ///#region 扩展属性


    ///#endregion


    ///#region 构造函数

    public final void setFK_Dept(String value) throws Exception {
        SetValByKey(DeptEmpAttr.FK_Dept, value);
        this.setMyPK(this.getFK_Dept() + "_" + this.getFK_Emp());
    }

    /**
     * 重写基类方法
     */
    @Override
    public Map getEnMap() {
        if (this.get_enMap() != null) {
            return this.get_enMap();
        }

        Map map = new Map("Port_DeptEmp");
        map.setEnDesc("部门人员信息");
        map.IndexField = DeptEmpAttr.FK_Dept;


        map.AddMyPK();
        map.AddTBString(DeptEmpAttr.FK_Dept, null, "部门", false, false, 1, 50, 1);
        map.AddDDLEntities(DeptEmpAttr.FK_Emp, null, "操作员", new BP.Port.Emps(), false);


        this.set_enMap(map);
        return this.get_enMap();
    }

    /**
     * 更新前做的事情
     *
     * @return
     * @throws Exception
     */
    @Override
    protected boolean beforeUpdateInsertAction() throws Exception {
        this.setMyPK(this.getFK_Dept() + "_" + this.getFK_Emp());
        return super.beforeUpdateInsertAction();
    }

    ///#endregion

    /**
     * UI界面上的访问控制
     *
     * @throws Exception
     */
    @Override
    public UAC getHisUAC() throws Exception {
        UAC uac = new UAC();
        uac.OpenForSysAdmin();
        return uac;
    }
}

package BP.GPM;

import BP.DA.*;
import BP.En.*;

import java.util.*;

/**
 * 部门岗位对应
 */
public class DeptStationAttr {

    ///#region 基本属性
    /**
     * 部门
     */
    public static final String FK_Dept = "FK_Dept";
    /**
     * 岗位
     */
    public static final String FK_Station = "FK_Station";

    ///#endregion
}
